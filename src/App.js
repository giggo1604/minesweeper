import React, { useState } from "react";
import styled from "@emotion/styled";
import useMinesweeper from "./useMinesweeper";
import useNumberInput from "./useNumberInput";

const ResetButton = styled.button({
  padding: 10
});

const GameWrapper = styled.div({
  padding: "2%",
  margin: "auto",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  "&>*": {
    marginBottom: 20
  }
});

const GameField = styled.div(props => ({
  display: "grid",
  gridTemplateColumns: `repeat(${props.size}, 20px)`
}));

const MineField = styled.button(props => ({
  height: 20,
  width: 20,
  fontWeight: 600,
  backgroundColor: "gray",
  fontFamily: "monospace",
  color: { 1: "blue", 2: "green", 3: "red", 4: "purple" }[props.bombCount],
  ...(props.revealed && { borderStyle: "none" }),
  ...(props.hasBomb &&
    process.env.NODE_ENV === "development" && { backgroundColor: "red" })
}));

const difficultyMap = {
  easy: { size: 10, bombs: 10 },
  medium: { size: 20, bombs: 30 },
  hard: { size: 50, bombs: 100 }
};

const App = () => {
  const [difficulty, setDifficulty] = useState("easy");
  const sizeInput = useNumberInput(difficultyMap[difficulty].size);
  const bombsInput = useNumberInput(difficultyMap[difficulty].bombs);

  const {
    fields,
    revealField,
    markField,
    resetGame,
    gameState
  } = useMinesweeper({ size: sizeInput.value, bombs: bombsInput.value });

  const handleDifficultyChange = e => {
    const difficulty = e.target.value;
    setDifficulty(difficulty);
    sizeInput.setValue(difficultyMap[difficulty].size);
    bombsInput.setValue(difficultyMap[difficulty].bombs);
  };

  const createHandleRevealClick = i => _ => revealField(i);
  const createHandleMarkClick = i => e => {
    e.preventDefault();
    markField(i);
  };

  return (
    <GameWrapper>
      <select value={difficulty} onChange={handleDifficultyChange}>
        <option value="easy">easy</option>
        <option value="medium">medium</option>
        <option value="hard">hard</option>
      </select>
      <div>
        <label>Size</label>
        <input min={10} type="number" {...sizeInput.inputHandle} />
      </div>
      <div>
        <label>Bombs</label>
        <input type="number" {...bombsInput.inputHandle} />
      </div>
      <div>{gameState}</div>
      <ResetButton onClick={resetGame}>Reset Game</ResetButton>
      <GameField size={sizeInput.value}>
        {fields.map((field, i) => (
          <MineField
            key={i}
            {...field}
            onClick={createHandleRevealClick(i)}
            onContextMenu={createHandleMarkClick(i)}
          >
            {field.revealed || gameState !== "playing"
              ? field.hasBomb
                ? "💥"
                : !!field.bombCount && field.bombCount
              : null}
            {field.marked && "🚩"}
          </MineField>
        ))}
      </GameField>
    </GameWrapper>
  );
};

export default App;
