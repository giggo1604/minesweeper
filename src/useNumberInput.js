import { useState } from "react";

const useNumberInput = initialValue => {
  const [value, setValue] = useState(initialValue);
  return {
    setValue,
    value,
    inputHandle: {
      value,
      onChange: e => setValue(Number.parseInt(e.target.value))
    }
  };
};

export default useNumberInput;
