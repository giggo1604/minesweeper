import { useState, useEffect } from "react";
import invariant from "tiny-invariant";

const useMinesweeper = ({ size = 10, bombs = 10 }) => {
  invariant(size * size > bombs, "Invalid game configuration!");

  const squaredSize = size * size;

  const getNeighbors = index => {
    const neighbors = [];
    for (const y of [-size, 0, size]) {
      for (const x of [-1, 0, 1]) {
        if (
          x + y !== 0 &&
          index + y + x >= 0 &&
          index + y + x < squaredSize &&
          Math.trunc((index + y + x) / size) === Math.trunc((index + y) / size)
        ) {
          neighbors.push(index + y + x);
        }
      }
    }
    return neighbors;
  };

  const seedFields = () => {
    const bombsMap = (function generateBombs(map = {}) {
      return Object.keys(map).length < bombs
        ? generateBombs(
            Object.assign(map, {
              [Math.floor(Math.random() * Math.floor(squaredSize + 1))]: true
            })
          )
        : map;
    })();

    return Array(squaredSize)
      .fill()
      .map((_, i) => ({
        revealed: false,
        marked: false,
        hasBomb: !!bombsMap[i],
        bombCount: getNeighbors(i).reduce(
          (s, n) => s + (bombsMap[n] ? 1 : 0),
          0
        )
      }));
  };

  const [fields, setFields] = useState(seedFields(size, bombs));

  const revealField = i =>
    setFields(fields => {
      const newFields = [...fields];
      const visitedMap = { [i]: true };
      const explorationStack = [i];

      while (explorationStack.length > 0) {
        const currentFieldIndex = explorationStack.pop();
        const currentField = fields[currentFieldIndex];

        newFields[currentFieldIndex] = {
          ...currentField,
          revealed: true,
          marked: false
        };

        if (!currentField.hasBomb && currentField.bombCount === 0) {
          const neighbors = getNeighbors(currentFieldIndex).filter(
            n => !visitedMap[n]
          );
          explorationStack.push(...neighbors);
          neighbors.forEach(n => {
            visitedMap[n] = true;
          });
        }
      }
      return newFields;
    });

  const markField = index =>
    setFields(fields => [
      ...fields.slice(0, index),
      {
        ...fields[index],
        marked: !fields[index].revealed && !fields[index].marked
      },
      ...fields.slice(index + 1, fields.length)
    ]);

  const resetGame = () => setFields(seedFields(size, bombs));

  const gameState = fields.find(field => field.revealed && field.hasBomb)
    ? "loss"
    : fields.find(field => !field.hasBomb && !field.revealed)
    ? "playing"
    : "won";

  useEffect(resetGame, [size, bombs]);

  return {
    fields,
    revealField,
    markField,
    resetGame,
    gameState
  };
};

export default useMinesweeper;
